int pinBuzzer = 7;

void setup() 
{
  pinMode(pinBuzzer, OUTPUT);
}

void loop() 
{
  tone(pinBuzzer, 622);
  delay(1000);
  tone(pinBuzzer, 466);
  delay(1000);
}
